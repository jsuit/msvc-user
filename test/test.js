/* eslint max-lines: "off" */
/* eslint max-statements: "off" */
/* eslint no-process-env: "off" */
/* global describe, it, after, before */

// During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const chakram = require('chakram');
const {expect} = chakram;
const uuidv1 = require('uuid/v1');
const jsonrpcLite = require('jsonrpc-lite');
const service = require('../service');
const {errors} = require('user-auth-jsonrpc');

function buildRequest(method, params) {
  const id = uuidv1();

  return jsonrpcLite.request(id, method, params);
}

describe('MSVC-USER', () => {
  let port = 0,
      url = '',
      schemaError = {};
  const HTTP200 = 200;

  before('start server', (done) => {
    service.start();
    service.app.on('ready', () => {
      ({port} = service.server.address());
      url = `http://localhost:${port}`;
      schemaError = {
        'type': 'object',
        'properties': {
          'jsonrpc': {
            'type': 'string',
            'pattern': '^2.0$'
          },
          'id': {
            'type': [
              'integer',
              'string'
            ]
          },
          'error': {
            'type': 'object',
            'properties': {
              'code': {'type': 'integer'},
              'message': {'type': 'string'},
              'data': {'type': 'object'}
            },
            'required': [
              'code',
              'message',
              'data'
            ],
            'additionalProperties': false
          }
        },
        'required': [
          'jsonrpc',
          'id',
          'error'
        ],
        'additionalProperties': false
      };
      console.log('service is ready');
      done();
    });
  });

  describe('/auth register', () => {
    it('it return 200 & missing parameter when email is undefined', () => {
      const jsonReq = buildRequest('register', {
        password: 'password',
        profile: {}
      });
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'email'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
    it('it return 200 & missing parameter when password is undefined', () => {
      const jsonReq = buildRequest('register', {
        email: 'user@domain.com',
        profile: {}
      });
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'password'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
    it('it return 200 & missing parameter when profile is undefined', () => {
      const jsonReq = buildRequest('register', {
        email: 'user@domain.com',
        password: 'password'
      });
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'profile'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
    it('it return 200 & missing parameter when profile is an empty object', () => {
      const jsonReq = buildRequest('register', {
        email: 'user@domain.com',
        password: 'password',
        profile: {}
      });
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'profile'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
    it('it return 200 & missing parameter when profile is not an object', () => {
      const jsonReq = buildRequest('register', {
        email: 'user@domain.com',
        password: 'password',
        profile: 'hello world!'
      });
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'profile'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
  });

  describe('/auth login', () => {
    it('it return 200 & unauthorized when no auth header', () => {
      const jsonReq = buildRequest('login');
      const options = {
        'headers': {'Content-Type': 'application/json'}
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.header('content-type', 'application/json; charset=utf-8');
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id, errors.unauthorized({})));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
  });

  describe('/auth updateProfile', () => {
    it('it return 200 & error when no bearer token is provided', () => {
      const jsonReq = buildRequest('updateProfile', {
        email: 'user@domain.com',
        profile: {}
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                errors.invalidJWS({})));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });

    it('it return 200 & error when no email parameter is provided', () => {
      const jsonReq = buildRequest('updateProfile', {
        profile: {}
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer token'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'email'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });

    it('it return 200 & error when no profile parameter is provided', () => {
      const jsonReq = buildRequest('updateProfile', {
        email: 'user@domain.net'
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer token'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'profile'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
  });

  describe('/auth updatePermission', () => {
    it('it return 200 & error when no bearer token is provided', () => {
      const jsonReq = buildRequest('updatePermission', {
        email: 'user@domain.org',
        permission: {}
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise.json(jsonrpcLite.error(jsonReq.id,
                                            errors.invalidJWS({})));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });

    it('it return 200 & error when no email parameter is provided', () => {
      const jsonReq = buildRequest('updatePermission', {
        permisson: {}
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer token'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'email'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });

    it('it return 200 & error when no permission parameter is provided', () => {
      const jsonReq = buildRequest('updatePermission', {
        email: 'user@domain.com'
      });
      const options = {
        'headers': {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer token'
        }
      };
      const response = chakram.post(`${url}/auth`, jsonReq, options);

      expect(response).to.have.status(HTTP200);
      expect(response).to.have.schema(schemaError);
      expect(response)
        .to.comprise
        .json(jsonrpcLite.error(jsonReq.id,
                                jsonrpcLite.JsonRpcError.invalidParams({
                                  parameter: 'permission'
                                })));
      // after(() => {console.log(response.valueOf().body)});
      return chakram.wait();
    });
  });

  after('stop service', (done) => {
    service.close();
    done();
  });
});
