const convict = require('convict');
const path = require('path');

// Define a schema
let config = convict({
    env: {
	doc: 'The application environment.',
	format: ['prod', 'dev', 'test'],
	default: 'dev',
	env: 'NODE_ENV',
	arg: 'env'
    },
    
    service: {
	port: {
	    doc: 'The port to bind.',
	    format: 'port',
	    default: 0,
	    env: 'PORT',
	    arg: 'port'
	}
    },

    db: {
	url: {
	    doc: 'The mongodb connection url.',
	    format: '*',
	    default: 'mongodb://localhost:27017/msvc_user',
	    arg: 'mongodb_url',
	    env: 'MONGODB_URL'
	}
    },

    mail: {
	sender: {
	    doc: 'The mailer sender.',
	    format: 'String',
	    default: "sender@domain",
	    env: 'MAIL_SENDER',
	    arg: 'mail_sender'
	},
	port: {
	    doc: 'The smtp port.',
	    format: 'port',
	    default: 25,
	    env: 'MAIL_PORT',
	    arg: 'mail_port'
	},
	host: {
	    doc: 'The smtp host.',
	    default: "",
	    format: 'String',
	    env: 'MAIL_HOST',
	    arg: 'mail_host'
	},
	user: {
	    doc: 'The smtp user.',
	    format: 'String',
	    default: "",
	    env: 'MAIL_USER',
	    arg: 'mail_user'
	},
	password: {
	    doc: 'The smtp user password.',
	    format: 'String',
	    default: "",
	    env: 'MAIL_PASSWORD',
	    arg: 'mail_password'
	},
	secure: {
	    doc: 'Use secure connection',
	    format: 'Boolean',
	    default: false,
	    env: 'MAIL_SECURE',
	    arg: 'mail_secure'
	},
	dirTemplates: {
	    doc: 'The directory where the email templates are located',
	    format: 'String',
	    default: path.join(__dirname, 'templates', 'mail'),
	    env: 'DIR_TEMPLATES',
	    arg: 'dir_templates'
	},
	templates: {
	    doc: 'The list of email templates availables',
	    format: 'Array',
	    default: ['confirm-register', 'confirm-password'],
	    env: 'MAIL_TEMPLATES',
	    arg: 'mail_templates'
	}
    }
});

let env = config.get('env');

config.loadFile(path.join(__dirname, 'config', env + '.json'));

// Perform validation
config.validate({allowed: 'strict'});

config.set('service.PATH_ROOT', __dirname);

if(config.get('env') === 'test') {
    process.env.NODE_ENV = 'test';
}

module.exports = config;
