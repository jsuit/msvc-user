const mailer = require('../tools/mailer');
const path = require('path');

mailer.connect({
    sender: 'sender@gmail.com',
    dirTemplates: path.join(__dirname, '../templates/mail'),
    templates: ['confirm-register', 'confirm-password']
}).then(_=>{
    return mailer.sendMail('josp.jorge@gmail.com', 'confirm-register', {
    })
}).then(infoSend => {
    console.log('Preview URL: %s', mailer.getTestMessageUrl(infoSend));
});
