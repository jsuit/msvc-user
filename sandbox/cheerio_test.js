const cheerio = require('cheerio');

const body = '\n<p>Hi,</p>\n<p>Thanks for signing up for <Service Name>!</p>\n<p>Please confirm your account at <a class=activate href="http://localhost:35726/auth/confirmRegister?email=user-test%40gmail.com&token=8dfb315c-68a3-43f1-a23f-397c0a7edcb6">activate</a></p>\n';

var $ = cheerio.load(body);

$(".activate").each(function() {
    var link = $(this);
    var text = link.text();
    var href = link.attr("href");

    console.log(text + " -> " + href);
});

console.log($(".activate").attr('href'));
